#pragma once

#include <Tools.hpp>
#include <Matrix.hpp>
#include <Function.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

namespace num_methods {
    using namespace std;
    using namespace fmt;

    double xi(double x0, unsigned i, double h) {
        return x0 + i * h;
    }

    double trapeze_integral(Function &fn, double a, double b, unsigned n) {
        double result = 0.0;
        double h = (b - a)/n;

        result += fn(a) + fn(b);

        for (unsigned i = 1; i < n; ++i)
            result += 2 * fn(xi(a, i, h));
    
        return h/2.0 * result;
    }

    double simpson13_integral(Function &fn, double a, double b, unsigned n) {
        expect(n % 2 == 0, "Number of points needs to be even");

        double result = 0.0;
        double h = (b - a)/n;

        result += fn(a) + fn(b);

        for (unsigned i = 1; i < n/2; ++i) {
            result += 2 * fn(xi(a, 2*i, h));
            result += 4 * fn(xi(a, 2*i - 1, h));
        
        }

        result += 4 * fn(xi(a, n - 1, h));
    
        return h/3.0 * result;
    }

    double simpson38_integral(Function &fn, double a, double b, unsigned n) {
        expect(n % 3 == 0, "Number of points needs to be multiple 3");

        double result = 0.0;
        double h = (b - a)/n;

        result += fn(a) + fn(b);
        for (unsigned i = 1; i < n/3; ++i) {
            result += 2 * fn(xi(a, 3 * i, h));
            result += 3 * (fn(xi(a, 3 * i - 2, h)) + fn(xi(a, 3 * i - 1, h)));
        }

        result += 3 * (fn(xi(a, n - 2, h)) + fn(xi(a, n - 1, h)));
        return 3.0*h/8.0 * result;
    }

    double romberg_integral_fixed(Function &fn, double a, double b, unsigned n) {
        Matrix r(Matrix<double>::zero(n, n));

        auto ij = [&r](unsigned i, unsigned j) {
            double cte = pow(4, j);
            double num = cte * r(i, j - 1) - r(i - 1, j - 1);
            double dem = cte - 1;
                    
            return num/dem;
        };

        for (unsigned i = 0; i < n; ++i)
            r(i, 0) = trapeze_integral(fn, a, b, pow(2, i));

        for (unsigned j = 1; j < n; ++j)
            for (unsigned i = j; i < n; ++i) 
                r(i, j) = ij(i, j);

        print("Approximations:\n{}\n", r);
        return r(n - 1, n - 1);
    }

    double romberg_integral_wtol(Function &fn, double a, double b, double tolerance = 0.0001) {
        size_t n = 2;
        Matrix r(Matrix<double>::zero(n, n));

        auto ij = [&r](unsigned i, unsigned j) {
            double cte = pow(4, j);
            double num = cte * r(i, j - 1) - r(i - 1, j - 1);
            double dem = cte - 1;
                    
            return num/dem;
        };

        for (unsigned i = 0; i < n; ++i)
            r(i, 0) = trapeze_integral(fn, a, b, pow(2, i));

        for (unsigned j = 1; j < n; ++j)
            for (unsigned i = j; i < n; ++i) 
                r(i, j) = ij(i, j);

        double error = fabs(r(1, 1) - r(0, 0));
        while (error > tolerance) {
            r.grow(n + 1, n + 1);
            n = r.no_rows();

            r(n - 1, 0) = trapeze_integral(fn, a, b, pow(2, n - 1));
            for (unsigned j = 1; j < n; ++j) 
                r(n - 1, j) = ij(n - 1, j);

            error = fabs(r(n - 1, n - 1) - r(n - 2, n - 2));
        }

        print("Approximations:\n{}\n", r);
        return r(n - 1, n - 1);
    }
}
