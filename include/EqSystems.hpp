#pragma once

#include <Tools.hpp>
#include <Matrix.hpp>
#include <Function.hpp>
#include <Equations.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

namespace num_methods::eqsystems {
    
    using namespace std;
    using namespace fmt;

    vector<double> regressive_sub(const Matrix<double>& matrix) {
        vector<double> solutions = {};

        for (long ii = matrix.no_rows() - 1; ii >= 0L; --ii) {
            double sum = 0;
            for (auto jj = 0; jj < solutions.size(); ++jj)
                sum += solutions[jj] * matrix(ii, matrix.no_cols() - jj - 2);

            auto solution = (matrix(ii, matrix.no_cols() - 1) - sum) / matrix(ii,ii);
            solutions.push_back(solution);
        }

        reverse(solutions.begin(), solutions.end());
        return solutions;
    }

    vector<double> progressive_sub(const Matrix<double>& matrix) {
        vector<double> solutions = {};

        for (auto ii = 0; ii < matrix.no_rows(); ++ii) {
            double sum = 0;
            for (auto jj = 0; jj < solutions.size(); ++jj)
                sum += solutions[jj] * matrix(ii, jj);

            auto solution = (matrix(ii, matrix.no_cols() - 1) - sum) / matrix(ii,ii);
            solutions.push_back(solution);
        }

        return solutions;
    }

    vector<double> gauss_simple(const Matrix<double>& a, const Matrix<double>& b) {
        auto eq_system = a.augmented(b);
        return regressive_sub(eq_system.stagger_simple());
    }

    vector<double> gauss_simple(const Matrix<double>& eq_system) {
        return regressive_sub(eq_system.stagger_simple());
    }

    vector<double> gauss_advanced(const Matrix<double>& a, const Matrix<double>& b) {
        auto eq_system = a.augmented(b);
        return regressive_sub(eq_system.stagger_advanced());
    }

    vector<double> gauss_advanced(const Matrix<double>& eq_system) {
        return regressive_sub(eq_system.stagger_advanced());
    }

    vector<double> lu_method(const Matrix<double>& a, const Matrix<double>& b) {
        auto lu = a.lu_decomposition();
        auto& L = get<0>(lu);
        auto& U = get<1>(lu);

        auto y_vec = progressive_sub(L.augmented(b));
        auto y_mat = Matrix<double>(y_vec.size(), 1);

        for (auto cc = 0; cc < y_vec.size(); ++cc)
            y_mat(cc, 0) = y_vec[cc];

        return regressive_sub(U.augmented(y_mat));
    }

    vector<double> plu_method(const Matrix<double>& a, const Matrix<double>& b) {
        auto plu = a.plu_decompotition();
        auto &P  = get<0>(plu);
        auto &L  = get<1>(plu);
        auto &U  = get<2>(plu);

        auto y_vec = progressive_sub(L.augmented(P * b));
        auto y_mat = Matrix<double>(y_vec.size(), 1);

        for (auto cc = 0; cc < y_vec.size(); ++cc)
            y_mat(cc, 0) = y_vec[cc];

        return regressive_sub(U.augmented(y_mat));
    }

    using NormaFunction = function<double (vector<double> const&)>;

    double norma1(const vector<double>& vec) {
        double sum = 0;
        for (auto& e : vec)
            sum += fabs(e);
        return sum;
    }

    vector<double> jacobi_method(
        const Matrix<double>& a,
        const Matrix<double>& b,
        vector<double> aproximations,
        double tolerance,
        NormaFunction norma = norma1) {

        expect(aproximations.size() == a.no_rows(), "Aproximations ill-formed");
        expect(a.no_rows() == b.no_rows(), "A and B differs in number of rows");

        vector<double> solutions(a.no_rows());
        double distance = 100;

        for (auto i = 0; comp::ge(distance, tolerance, 15) ; ++i) {
            for (size_t r = 0; r < a.no_rows(); ++r) {
                expect(!comp::eq(a(r,r), 0.0, 6), "Pivot element is zero!");

                auto sum = 0.0;
                for (auto j = 0; j < a.no_cols(); ++j)
                    if (r != j) sum += a(r, j) * aproximations[j];

                solutions[r] = (b(r, 0) - sum) / a(r,r);
            }

            distance = fabs(norma(solutions) - norma(aproximations));
            aproximations = solutions;
        }

        return solutions;
    }

    vector<double> gauss_seidel(
        const Matrix<double>& a,
        const Matrix<double>& b,
        vector<double> aproximations,
        double tolerance,
        NormaFunction norma = norma1) {

        expect(aproximations.size() == a.no_rows(), "Aproximations ill-formed");
        expect(a.no_rows() == b.no_rows(), "A and B differs in number of rows");

        vector<double> solutions(a.no_rows());
        double distance = 100;

        print(cout, "k, x_k, y_k, z_k, || X_k - X_k-1 ||\n");
        print(cout, "0, ");
        for (auto& a: aproximations)
            print(cout, "{:.6f}, ", a);
        print(cout, "{:.6f}\n", distance);

        for (auto i = 0; comp::ge(distance, tolerance, 15) ; ++i) {
            for (size_t r = 0; r < a.no_rows(); ++r) {
                expect(!comp::eq(a(r,r), 0.0, 6), "Pivot element is zero!");

                auto sum = 0.0;
                for (auto j = 0; j < r; ++j)
                    if (r != j) sum += a(r, j) * solutions[j];

                for (auto j = r; j < a.no_cols(); ++j)
                    if (r != j) sum += a(r, j) * aproximations[j];

                solutions[r] = (b(r, 0) - sum) / a(r,r);
            }

            distance = fabs(norma(solutions) - norma(aproximations));
            aproximations = solutions;
            print(cout, "{}, ", i+1);
            for (auto& s: solutions)
                print(cout, "{:.6f}, ", s);
            print(cout, "{:.6f}\n", distance);
        }

        return solutions;
    }

    using FunctionMatrix = Matrix<Function>;

    Matrix<double> eval_matrix(const FunctionMatrix& fmat, const vector<double>& val) {
        expect(!fmat.empty(), "Matrix function is empty!");

        Matrix<double> result(fmat.no_rows(), fmat.no_cols());
        fmat.map([&fmat, &result, &val](auto i, auto j){
            auto fn = fmat.clone(i,j);
            result(i,j) = fn(val);
        });

        return result;
    }

    Matrix<double> calculate_jacobian(const FunctionMatrix& fmat, const vector<double>& val) {
        expect(!fmat.empty(), "Matrix function is empty!");
        expect(fmat.no_cols() == 1, "Too high dimension");

        unsigned short dim = fmat(0,0).get_dim();
        for (int i = 1; i < fmat.no_rows(); ++i)
            expect(fmat(i,0).get_dim() == dim, "Dimensions differs");

        Matrix<double> jacobian(fmat.no_rows(), dim);
        for (int i = 0; i < jacobian.no_rows(); ++i) {
            auto fn = fmat.clone(i, 0);
            for (int j = 0; j < jacobian.no_cols(); ++j)
                jacobian(i, j) = equations::partial_derivative(fn, val, j);
        }

        return jacobian;
    }

    vector<double> newton_method(const FunctionMatrix& F, const FunctionMatrix& JF, vector<double> aproximation, double tolerance = 0.001, NormaFunction norma = norma1) {
        double distance = 100;
        vector<double> solution(aproximation.size());
        vector<double> last_apr(aproximation.size());

        for (auto i = 0; distance >= tolerance; ++i) {
            auto evalued_function = eval_matrix( F, aproximation);
            auto evalued_jacobian = eval_matrix(JF, aproximation);
            solution = plu_method(evalued_jacobian, evalued_function);
            last_apr = aproximation;
            for (int i = 0; i < aproximation.size(); ++i)
                aproximation[i] = aproximation[i] - solution[i];
            distance = fabs(norma(aproximation) - norma(last_apr));
        }

        return aproximation;
    }

    vector<double> newton_method(const FunctionMatrix& F, vector<double> aproximation, double tolerance = 0.001, NormaFunction norma = norma1) {
        double distance = 100;
        vector<double> solution(aproximation.size());
        vector<double> last_apr(aproximation.size());

        for (auto i = 0; distance >= tolerance; ++i) {
            auto evalued_function = eval_matrix(F, aproximation);
            auto evalued_jacobian = calculate_jacobian(F, aproximation);
            solution = plu_method(evalued_jacobian, evalued_function);
            last_apr = aproximation;
            for (int i = 0; i < aproximation.size(); ++i)
                aproximation[i] = aproximation[i] - solution[i];
            distance = fabs(norma(aproximation) - norma(last_apr));
        }

        return aproximation;
    }

}
