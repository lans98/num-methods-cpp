#ifndef TOOLS_NUM_METHODS_HPP
#define TOOLS_NUM_METHODS_HPP

#include <cmath>
#include <string>
#include <functional>

#define getter(name) auto const& name()
#define setter(name) void name(decltype(m_##name) value)
#define selfify(this) auto& self = *this

namespace num_methods {

    using namespace std;

    using SimpleLambda = std::function<void (void)>;
    using MatrixLambda = std::function<void (std::size_t, std::size_t)>;
    using SummationLambda = std::function<double (long)>;

    void expect(bool condition, string throw_msg)  {
        if (!condition) throw std::runtime_error(throw_msg);
    }
}

namespace num_methods::wrappers {

    double summation(long start, long end, const SummationLambda& term) {
        double sum = 0;
        for (; start <= end; ++start)
            sum += term(start);
        return sum;
    }

}

namespace num_methods::comp {

    bool eq(double a, double b, int accuracy = 6) {
        // Check integral parts first
        long int_a = static_cast<long>(a);
        long int_b = static_cast<long>(b);

        // If integral parts aren't equal, return false
        if (int_a != int_b) return false;

        // If they are equal, we can now check if it's decimal parts are equal
        long dec_a = (a - int_a) * (std::pow(10, accuracy));
        long dec_b = (b - int_b) * (std::pow(10, accuracy));
        return dec_a == dec_b;
    }

    bool lt(double a, double b, int accuracy = 6) {
        // Check integral parts first
        long int_a = static_cast<long>(a);
        long int_b = static_cast<long>(b);

        // If a isn't less or equal b, return false
        if (int_a > int_b) return false;
        if (int_a < int_b) return true;

        long dec_a = (a - int_a) * (std::pow(10, accuracy));
        long dec_b = (b - int_b) * (std::pow(10, accuracy));
        return dec_a < dec_b;
    }

    bool gt(double a, double b, int accuracy = 6) {
        // Check integral parts first
        long int_a = static_cast<long>(a);
        long int_b = static_cast<long>(b);

        // If a isn't greater or equal b, return false
        if (int_a < int_b) return false;
        if (int_a > int_b) return true;

        long dec_a = (a - int_a) * (std::pow(10, accuracy));
        long dec_b = (b - int_b) * (std::pow(10, accuracy));
        return dec_a > dec_b;
    }

    bool le(double a, double b, int accuracy = 6) {
        // Check integral parts first
        long int_a = static_cast<long>(a);
        long int_b = static_cast<long>(b);

        // If a isn't less or equal b, return false
        if (int_a > int_b) return false;
        if (int_a < int_b) return true;

        long dec_a = (a - int_a) * (std::pow(10, accuracy));
        long dec_b = (b - int_b) * (std::pow(10, accuracy));
        return dec_a <= dec_b;
    }

    bool ge(double a, double b, int accuracy = 6) {
        // Check integral parts first
        long int_a = static_cast<long>(a);
        long int_b = static_cast<long>(b);

        // If a isn't greater or equal b, return false
        if (int_a < int_b) return false;
        if (int_a > int_b) return true;

        long dec_a = (a - int_a) * (std::pow(10, accuracy));
        long dec_b = (b - int_b) * (std::pow(10, accuracy));
        return dec_a >= dec_b;
    }
}

#endif
