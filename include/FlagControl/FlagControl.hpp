#ifndef FLAGCONTROL_LIBRARY_H
#define FLAGCONTROL_LIBRARY_H

#include <map>
#include <string>

namespace FlagControl {

using FlagContainer = int;

class FlagController {
private:
  std::map<std::string, FlagContainer> flags;
  std::string default_flag_name;
  std::string current_flag_name;

public:
  explicit FlagController(FlagContainer flag = 0):
    default_flag_name("default"),
    current_flag_name(default_flag_name) {
    flags.emplace(current_flag_name, flag);
  }

  explicit FlagController(std::string default_flag_name, FlagContainer flag = 0):
    default_flag_name(std::move(default_flag_name)),
    current_flag_name(default_flag_name) {
    flags.emplace(default_flag_name, flag);
  }

  FlagContainer& operator[](const std::string& index) { return flags.at(index); }

  void using_container(const std::string &flag_name) {
    if (flags.find(flag_name) != flags.end())
      current_flag_name = flag_name;
  }

  void using_default_container() { current_flag_name = default_flag_name; }

  void set_flag(const std::string &flag_name, FlagContainer flag) { flags.at(flag_name) = flag; }
  void set_flag(FlagContainer flag) { set_flag(current_flag_name, flag); }

  void add_flag(const std::string &flag_name, FlagContainer flag) { flags.at(flag_name) |= flag; }
  void add_flag(FlagContainer flag) { add_flag(current_flag_name, flag); }

  void rmv_flag(const std::string &flag_name, FlagContainer flag) { flags.at(flag_name) &= ~flag; }
  void rmv_flag(FlagContainer flag) { rmv_flag(current_flag_name, flag); }

  bool has_flag(const std::string &flag_name, FlagContainer flag) { return (flags.at(flag_name) & flag) == flag; }
  bool has_flag(FlagContainer flag) { return has_flag(current_flag_name, flag); }

  FlagContainer has_at_least(const std::string &flag_name, FlagContainer flag) { return (flags.at(flag_name) & flag); }
  FlagContainer has_at_least(FlagContainer flag) { return has_at_least(current_flag_name, flag); }

  void create_container(const std::string &flag_name, FlagContainer flag = 0) { flags.emplace(flag_name, flag); }
  void delete_container(const std::string &flag_name) {
    if (flag_name == current_flag_name || flag_name == default_flag_name)
      throw std::runtime_error("You cannot delete your current/default flag container!");
    else
      flags.erase(flag_name);
  }
};

}

#endif
