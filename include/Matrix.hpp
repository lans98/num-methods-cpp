#pragma once

#include <Tools.hpp>

#include <vector>
#include <memory>
#include <ostream>
#include <utility>
#include <algorithm>
#include <type_traits>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

namespace num_methods {

    using namespace std;
    using namespace fmt;

    template <class T>
    class Matrix {
    private:
        vector<vector<T>> m_rows;
        size_t m_no_rows;
        size_t m_no_cols;

    public:
        Matrix() = delete;

        Matrix(vector<vector<T>> matrix) {
            selfify(this);
            auto cols = matrix[0].size();
            for (auto &row : matrix)
                expect(row.size() == cols, "Number of columns differ");

            self.m_rows = move(matrix);
            self.m_no_rows = self.m_rows.size();
            self.m_no_cols = cols;
        }

        Matrix(vector<T> row_vector): m_rows(1) {
            selfify(this);
            self.m_rows[0] = row_vector;
            self.m_no_rows = 1;
            self.m_no_cols = row_vector.size();
        }

        Matrix(size_t rows, size_t cols): m_rows(rows), m_no_rows(rows), m_no_cols(cols) {
            vector<T> temp(cols);

            for (auto& row : m_rows)
                row = temp;
        }

        Matrix(const Matrix& matrix):
            m_rows(matrix.m_rows),
            m_no_rows(matrix.m_no_rows),
            m_no_cols(matrix.m_no_cols) {}

        Matrix(Matrix&& matrix):
            m_rows(move(matrix.m_rows)),
            m_no_rows(matrix.m_no_rows),
            m_no_cols(matrix.m_no_cols) {}

        static Matrix zero(size_t rows, size_t cols) {
            static_assert(
                is_arithmetic<T>::value,
                "T isn't an arithmetic type to do this! (integer or float)"
            );

            Matrix result(rows, cols);
            for (auto& row : result.m_rows)
                for (auto& e : row)
                    e = static_cast<T>(0);

            return result;
        }

        static Matrix eye(size_t size) {
            static_assert(
                is_arithmetic<T>::value,
                "T isn't an arithmetic type to do this! (integer or float)"
            );

            Matrix result(size, size);
            for (auto i = 0, j = 0; i < size; ++i, ++j)
                result(i,j) = static_cast<T>(1);
            return result;
        }

        getter(no_rows) const { return m_no_rows; }
        getter(no_cols) const { return m_no_cols; }

        T& operator()(size_t row, size_t col) { return m_rows[row][col]; }
        const T& operator()(size_t row, size_t col) const { return m_rows[row][col]; }

        vector<T>& operator()(size_t row) { return m_rows[row]; }
        const vector<T>& operator()(size_t row) const { return m_rows[row]; }

        T clone(size_t row, size_t col) const { return m_rows[row][col]; }
        vector<T> clone(size_t row) const { return m_rows[row]; }

        Matrix operator+(const Matrix& matrix) const {
            selfify(this);

            expect(
                self.m_no_rows == matrix.m_no_rows && self.m_no_cols == matrix.m_no_cols,
                "Can't perform sum between matrices of different size"
            );


            Matrix result(m_no_rows, m_no_cols);
            for (size_t r = 0; r < m_no_rows; ++r)
                for (size_t c = 0; c < m_no_cols; ++c)
                    result(r, c) = self(r,c) + matrix(r,c);

            return result;
        }

        Matrix operator*(const Matrix& matrix) const {
            selfify(this);
            expect(self.m_no_cols == matrix.m_no_rows, "Argument matrix and this matrix differ in no. rows and cols");

            Matrix result(self.m_no_rows, matrix.m_no_cols);

            for (auto ii = 0; ii < self.m_no_rows; ++ii)
                for (auto jj = 0; jj < matrix.m_no_cols; ++jj)
                    result(ii, jj) = wrappers::summation(0, self.m_no_rows - 1, [&self, &matrix, ii, jj](auto i){
                        return self(ii, i) * matrix(i, jj);
                    });

            return result;
        }

        Matrix operator*(const T& scalar) const {
            selfify(this);
            static_assert(
                is_arithmetic<T>::value,
                "T isn't an arithmetic type to do this! (integer or float)"
            );

            Matrix result(self.m_no_rows, self.m_no_cols);

            for (auto ii = 0; ii < result.m_no_rows; ++ii)
                for (auto jj = 0; jj < result.m_no_cols; ++jj)
                    result(ii,jj) = self(ii,jj) * scalar;

            return result;
        }

        void map(const MatrixLambda& lambda) const {
            for (size_t i = 0; i < m_no_rows; ++i)
                for (size_t j = 0; j < m_no_cols; ++j)
                    lambda(i, j);
        }

        void grow(size_t rows, size_t cols) {
            selfify(this);

            expect(
                rows >= self.m_no_rows && cols >= self.m_no_cols, 
                "This method only works for growing, use resize/shrink instead"
            );

            auto push_rows = [&self](size_t rows) {
                for (auto i = self.m_no_rows; i < rows; ++i)
                    self.m_rows.push_back(vector<T>(self.m_no_cols, static_cast<T>(0)));

                self.m_no_rows = rows;
            };

            auto push_cols = [&self](size_t cols) {
                for (auto& row : self.m_rows)
                    for (auto i = self.m_no_cols; i < cols; ++i)
                        row.push_back(static_cast<T>(0));

                self.m_no_cols = cols;
            };

            if (rows > self.m_no_rows && cols > self.m_no_cols) {
                push_rows(rows);
                push_cols(cols);
            } else if (rows > self.m_no_rows && cols == self.m_no_cols) {
                push_rows(rows);
            } else if (rows == self.m_no_rows && cols > self.m_no_cols) {
                push_cols(cols);
            } 
        }

        void shrink(size_t rows, size_t cols) {
            selfify(this);

            expect(
                rows <= self.m_no_rows && cols <= self.m_no_cols,
                "This method only works for shrinking, use resize/grow instead"
            );

            auto erase_rows = [&self](size_t rows) {
                auto beg = self.m_rows.begin();
                auto end = self.m_rows.begin() + self.m_rows.size();
                self.m_rows.erase(beg + rows, end);

                self.m_no_rows = rows;
            };

            auto erase_cols = [&self](size_t cols) {
                for (auto& row : self.m_rows) {
                    auto beg = row.begin();
                    auto end = row.begin() + row.size();
                    row.erase(beg + cols, end);
                }

                self.m_no_cols = cols;
            };

            if (rows < self.m_no_rows && cols < self.m_no_cols) {
                erase_rows(rows);
                erase_cols(cols);
            } else if (rows == self.m_no_rows && cols < self.m_no_cols) {
                erase_cols(cols);
            } else if (rows < self.m_no_rows && cols == self.m_no_cols) {
                erase_rows(rows);
            }
        }

        void resize(size_t rows, size_t cols) {
            selfify(this);

            if (rows >= self.m_no_rows && cols >= self.m_no_cols) {
                grow(rows, cols);
            } else if (rows <= self.m_no_rows && cols <= self.m_no_cols) {
                shrink(rows, cols);
            } else if (rows >= self.m_no_rows && cols <= self.m_no_cols) {
                shrink(self.m_no_rows, cols);
                grow(rows, self.m_no_cols);
            } else if (rows <= self.m_no_rows && cols >= self.m_no_cols) {
                shrink(rows, self.m_no_cols);
                grow(self.m_no_rows, cols);
            }
        }

        Matrix augmented(const Matrix& matrix) const {
            selfify(this);
            expect(matrix.m_no_rows == self.m_no_rows, "Argument matrix has different number of rows");

            Matrix result(self);
            result.m_no_cols += matrix.m_no_cols;

            for (auto ii = 0; ii < result.m_no_rows; ++ii)
                for (auto& elem : matrix(ii))
                    result(ii).push_back(elem);

            return result;
        }

        Matrix stagger_simple() const {
            selfify(this);

            Matrix result(self);
            for (auto rr = 0; rr < self.m_no_rows; ++rr) {
                auto& pivot_row = result(rr);
                auto& pivot_elm = result(rr, rr); // elm -> element
                for (auto ii = rr + 1; ii < self.m_no_rows; ++ii) {
                    auto& curr_row = result(ii);
                    auto& temp_elm = result(ii, rr);

                    auto m = temp_elm/pivot_elm;
                    for (auto jj = 0; jj < self.m_no_cols; ++jj)
                        curr_row[jj] -= m * pivot_row[jj];
                }
            }

            return result;
        }

        Matrix stagger_advanced() const {
            selfify(this);

            Matrix result(self);

            for (auto rr = 0; rr < result.m_no_rows; ++rr) {
                // Search for the greatest element in the column rr
                auto max_rr = rr; // index of the maximum element
                auto max_el = fabs(result(rr, max_rr));
                for (auto ii = max_rr + 1; ii < result.m_no_rows; ++ii) {
                    if (fabs(result(ii, rr)) > max_el) {
                        max_rr = ii;
                        max_el = fabs(result(ii, rr));
                    }
                }

                print("Find max element for pivot, max = {:.6f} at row {}\n", max_el, max_rr);
                print("Now swap if necessary\n");

                if (max_rr != rr) result.swap_rows(rr, max_rr);

                // Classical algorithm starts here
                auto& pivot_row = result(rr);
                auto& pivot_elm = result(rr, rr); // elm -> element
                print("Select {} as pivot row, and {:.6f} as pivot element\n", rr, pivot_elm);
                for (auto ii = rr + 1; ii < result.m_no_rows; ++ii) {
                    auto& curr_row = result(ii);
                    auto& temp_elm = result(ii, rr);

                    auto m = temp_elm/pivot_elm;
                    print("Calculate m({},{}) = {:.6f}\n", ii, rr, m);
                    for (auto jj = 0; jj < result.m_no_cols; ++jj)
                        curr_row[jj] -= m * pivot_row[jj];
                    print("Result:\n{}", result);
                }
            }

            print("Nothing more to do\n");
            return result;
        }

        auto lu_decomposition() const {
            selfify(this);

            Matrix L(Matrix::eye(m_no_rows));
            Matrix U(self);

            for (auto rr = 0; rr < U.m_no_rows; ++rr) {
                auto& pivot_row = U(rr);
                auto& pivot_elm = U(rr, rr); // elm -> element
                for (auto ii = rr + 1; ii < U.m_no_rows; ++ii) {
                    auto& curr_row = U(ii);
                    auto& temp_elm = U(ii, rr);

                    if (pivot_elm == 0.0)
                        throw runtime_error("This matrix doesn't have LU decomposition");

                    auto m = temp_elm/pivot_elm;
                    L(ii, rr) = m;
                    for (auto jj = 0; jj < U.m_no_cols; ++jj)
                        curr_row[jj] -= m * pivot_row[jj];
                }
            }

            return make_tuple(L, U);
        }

        auto plu_decompotition() const {
            selfify(this);

            Matrix P(Matrix::eye(m_no_rows));
            Matrix L(Matrix::zero(m_no_rows, m_no_rows));
            Matrix U(self);

            for (auto rr = 0; rr < U.m_no_rows; ++rr) {
                // Search for the greatest element in the column rr
                auto max_rr = rr; // index of the maximum element
                auto max_el = fabs(U(rr, max_rr));
                for (auto ii = max_rr + 1; ii < U.m_no_rows; ++ii) {
                    if (fabs(U(rr, ii)) > max_el) {
                        max_rr = ii;
                        max_el = fabs(U(rr, ii));
                    }
                }

                if (max_rr != rr) {
                     U.swap_rows(rr, max_rr);
                     P.swap_rows(rr, max_rr);
                     L.swap_rows(rr, max_rr);
                 }

                // Classical algorithm starts here
                auto& pivot_row = U(rr);
                auto& pivot_elm = U(rr, rr); // elm -> element
                for (auto ii = rr + 1; ii < U.m_no_rows; ++ii) {
                    auto& curr_row = U(ii);
                    auto& temp_elm = U(ii, rr);

                    auto m = temp_elm/pivot_elm;
                    L(ii, rr) = m;
                    for (auto jj = 0; jj < U.m_no_cols; ++jj)
                        curr_row[jj] -= m * pivot_row[jj];
                }
            }

            return make_tuple(P, L + Matrix::eye(m_no_rows), U);
        }

        bool is_square() const { return m_no_cols == m_no_rows; }

        void swap_rows(size_t i, size_t j) {
            selfify(this);
            if (i >= self.m_no_rows || j >= self.m_no_rows)
                throw runtime_error("Index(s) aren't in range");

            swap(self.m_rows[i], self.m_rows[j]);
        }

        void swap_cols(size_t i, size_t j) {
            selfify(this);
            if (i >= self.m_no_cols || j >= self.m_no_cols)
                throw runtime_error("Index(s) aren't in range");

            for (auto &row: self.m_rows)
                swap(row[i], row[j]);
        }

        Matrix transpose() {
            selfify(this);

            Matrix temp(self.no_cols(), self.no_rows());

            for (size_t i = 0; i < self.no_rows(); ++i)
                for (size_t j = 0; j < self.no_cols(); ++j)
                    temp(j,i) = self(i,j);

            return temp;
        }

        auto size() const { return make_pair(m_no_rows, m_no_cols); }
        bool empty() const { return m_no_rows == 0 && m_no_cols == 0; }

        friend ostream& operator<<(ostream& out, const Matrix& matrix) {
            for (auto &r : matrix.m_rows) {
                for (auto &c : r)
                    print(out, "{:>12.6f} ", c);
                print(out, "\n");
            }

            return out;
        }
    };
}
