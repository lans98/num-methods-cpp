#pragma once

#include <Poly.hpp>
#include <Tools.hpp>
#include <Matrix.hpp>
#include <Function.hpp>
#include <Equations.hpp>
#include <EqSystems.hpp>
#include <Interpolation.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

