#include <Poly.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace std;
using namespace fmt;
using namespace num_methods;

class A {};

int main() {
    vector<double> va(2);

    va[0] = 1;
    va[1] = 3;
    Poly<double> a(va);

    va[0] = 1;
    va[1] = -2;
    Poly<double> b(va);

    auto result = a * b;
    print("{}\n", result);

    result = result / 2;
    print("{}\n", result);

    result = result * 2;
    print("{}\n", result);

    Poly<double> c(0, 1);
    print("{}\n", c);

    va[0] = 4;
    va[1] = 2;
    Poly<double> d(va);
    print("{}\n", d);
    d.increase_grade_to(3);
    print("{}\n", d);
}

