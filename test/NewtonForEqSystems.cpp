#include <Function.hpp>
#include <NumMethods.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace std;
using namespace fmt;
using namespace num_methods;
using namespace num_methods::eqsystems;

int main() {
    {
        FunctionMatrix F(2,1);
        F(0,0) = Function("x^2 + y^2 - 2", "x,y");
        F(1,0) = Function(    "x - y - 1", "x,y");

        FunctionMatrix JF(2,2);
        JF(0,0) = Function("2*x", "x,y");
        JF(0,1) = Function("2*y", "x,y");
        JF(1,0) = Function(  "1", "x,y");
        JF(1,1) = Function( "-1", "x,y");

        vector<double> aproximation(2);
        aproximation[0] =  1;
        aproximation[1] =  2;

        auto result = Matrix(newton_method(F, JF, aproximation, 0.0001));
        print("Solutions case 1:\n{}\n", result);
    }

    {
        FunctionMatrix F(3,1);
        F(0,0) = Function("x^2 + y^2 + z^2 - 2", "x,y,z");
        F(1,0) = Function(      "x + y + z - 1", "x,y,z");
        F(2,0) = Function(      "x^2 + y^2 - z", "x,y,z");

        FunctionMatrix JF(3,3);
        JF(0,0) = Function("2*x", "x,y,z");
        JF(0,1) = Function("2*y", "x,y,z");
        JF(0,2) = Function("2*z", "x,y,z");
        JF(1,0) = Function(  "1", "x,y,z");
        JF(1,1) = Function(  "1", "x,y,z");
        JF(1,2) = Function(  "1", "x,y,z");
        JF(2,0) = Function("2*x", "x,y,z");
        JF(2,1) = Function("2*y", "x,y,z");
        JF(2,2) = Function( "-1", "x,y,z");

        vector<double> aproximation(3);
        aproximation[0] =  0.2;
        aproximation[1] =  0.1;
        aproximation[2] =  0.1;

        auto result = Matrix(newton_method(F, JF, aproximation, 0.0001));
        print("Solutions case 2:\n{}\n", result);
    }

    {
        FunctionMatrix F(2,1);
        F(0,0) = Function("x^2 + y^2 - 1", "x,y");
        F(1,0) = Function(     "-x^2 + y", "x,y");

        vector<double> aproximation(2);
        aproximation[0] = 1;
        aproximation[1] = 0;

        auto result = Matrix(newton_method(F, aproximation, 0.0001));
        print("Solutions case 3:\n{}\n", result);
    }

    {
        FunctionMatrix F(2,1);
        F(0,0) = Function("x^2 + y^2 - 2", "x,y");
        F(1,0) = Function(    "x - y - 1", "x,y");

    }
}
