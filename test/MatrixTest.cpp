#include <Matrix.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace std;
using namespace num_methods;

void test1() {
    Matrix<double> m(3,3);

    print("cols: {}, rows: {}\n", m.no_cols(), m.no_rows());
    print("\n{}", m);
}

void test2() {
    Matrix<double> a(3,3);
    a.map([&a](auto i, auto j){
        a(i,j) = j;
    });

    print("\n{}", a);

    Matrix<double> b(3,1);
    b(0,0) = 3;
    b(1,0) = 5;
    b(2,0) = 7;

    print("\n{}", b);

    auto result = a.augmented(b);
    print("\n{}", result);
    print("size: {}x{}\n", result.no_rows(),result.no_cols());
}

void test3() {
    Matrix<double> identity(Matrix<double>::eye(3));
    print("\n{}", identity);
}

void test4() {
    Matrix<double> lu_test({
        { 3, 2,  1},
        { 5, 1, -6},
        { 2, 9, -4}
    });

    auto lu = lu_test.lu_decomposition();
    auto& L = get<0>(lu);
    auto& U = get<1>(lu);

    print("\n{}", lu_test);
    print("\n{}", L);
    print("\n{}", U);
    print("\n{}", L * U);
}

void test5() {
    Matrix<double> a({
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    });

    Matrix<double> b(a);

    auto result = a * b;
    print("\n{}", result);
}

void test6() {
    Matrix<double> a({
        {1, 3, 2},
        {2, -4, 5},
        {3, 7, 10}
    });

    Matrix<double> b(3, 1);
    b(0,0) = 6;
    b(0,1) = 3;
    b(0,2) = 20;

    auto simple_stagged  = a.augmented(b).stagger_simple();
    auto advance_stagged = a.augmented(b).stagger_advanced();

    print("\n{}", simple_stagged);
    print("\n{}", advance_stagged);
}

void test7() {
    Matrix<double> plu_test({
        {0, 2,  1},
        {5, 1, -6},
        {2, 9, -4}
    });

    auto plu = plu_test.plu_decompotition();
    auto& P  = get<0>(plu);
    auto& L  = get<1>(plu);
    auto& U  = get<2>(plu);

    print("\n{}", plu_test);
    print("\n{}", P);
    print("\n{}", L);
    print("\n{}", U);
    print("\n{}", P * plu_test);
    print("\n{}", L * U);
}

void test8() {
    Matrix<double> vertical_vector({
        vector<double>({1}),
        vector<double>({2}),
        vector<double>({3})
    });

    print("\n{}", vertical_vector);
    Matrix<double> horizontal_vector(vertical_vector.transpose());
    print("\n{}", horizontal_vector);
}

void test9() {
    Matrix<double> matrix({
        { 1, 2, 3 },
        { 4, 5, 6 },
        { 7, 8, 9 }
    });

    print("Test 9:\n");
    print("\n{}\nsize: {}x{}", matrix, matrix.no_rows(), matrix.no_cols());
    matrix.grow(4, 4);
    print("\n{}\nsize: {}x{}", matrix, matrix.no_rows(), matrix.no_cols());
    matrix.shrink(2, 2);
    print("\n{}\nsize: {}x{}", matrix, matrix.no_rows(), matrix.no_cols());
    matrix.resize(3, 3);
    print("\n{}\nsize: {}x{}", matrix, matrix.no_rows(), matrix.no_cols());
}

int main(int argc, char** argv) {
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    test9();
}
