#include <Integrals.hpp>

using namespace num_methods;

void test1() {
    Function f1("x^2", "x");
    double intg = simpson13_integral(f1, 0, 1, 2);
    print("Simpson con 2 puntos: {}\n", intg);
    intg = simpson13_integral(f1, 0, 1, 4);
    print("Simpson con 4 puntos: {}\n", intg);
    intg = simpson13_integral(f1, 0, 1, 40);
    print("Simpson con 40 puntos: {}\n", intg);
}

int main() {
    test1();
}
