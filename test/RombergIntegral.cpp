#include <Integrals.hpp>

using namespace num_methods;

void test1() {
    print("Test 1:\n");
    Function f1("x^2", "x");
    auto intg = romberg_integral_fixed(f1, 0, 1, 4);
}

void test2() {
    print("Test 2:\n");
    Function f1("sin(x)", "x");
    auto intg = romberg_integral_fixed(f1, 0, 3.141592653589793/2, 4);
}

void test3() {
    print("Test 3:\n");
    Function f1("x^2", "x");
    auto intg = romberg_integral_wtol(f1, 0, 1, 0.0001);
}

void test4() {
    print("Test 4:\n");
    Function f1("sin(x)", "x");
    auto intg = romberg_integral_wtol(f1, 0, 3.141592653589793/2, 0.0001);
}


int main() {
    test1();
    test2();
    test3();
    test4();
}
