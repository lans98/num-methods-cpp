#include <Interpolation.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace std;
using namespace fmt;
using namespace num_methods;

void test1() {
    vector<double> x { 2, 3, 5 };
    vector<double> y { 4, 1, 2 };
    auto poly = newton_interpolation(x, y);
    print("{}\n", poly);
    
    vector<double> a;
    for (auto& e:x)
        a.push_back(poly(e));

    print("Original y's: ");
    for (auto& e:y)
        print("{} ", e);

    print("\nSolved y's: ");
    for (auto& e:a)
        print("{} ", e);
    print("\n\n");
}

int main() {
    test1();
}
