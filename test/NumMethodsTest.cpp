#include <NumMethods.hpp>
#include <FlagControl.hpp>

#include <cstring>
#include <iostream>
#include <fstream>
#if defined _WIN32 || defined _WIN64 || defined __WIN32__
#include "GetOptWindows/getopt.h"
#else
#include <getopt.h>
#endif

static int flags = 0;

enum FLAGS {
    FUNCTION_GIVEN  = 0x0001,
    STEPS_GIVEN         = 0x0002,
    BEGIN_GIVEN         = 0x0004,
    END_GIVEN               = 0x0008,
    VERBOSE                 = 0x0010,
    FIRST_METHOD        = 0x0020,
    SECOND_METHOD       = 0x0040,
    THIRD_METHOD        = 0x0080,
    OUTPUT_GIVEN        = 0x0100,
    METHOD_GIVEN        = 0x0200,
    TOLERANCE_GIVEN = 0x0400,
    LINEAR_METHOD       = 0x0800,
    REGULA_METHOD       = 0x1000,

    NEEDED_OPTIONS  = FUNCTION_GIVEN | BEGIN_GIVEN | END_GIVEN
};



static option long_options[] = {
    {"function"  , required_argument , 0 , 'f'},
    {"steps"         , required_argument , 0 , 's'},
    {"begin"         , required_argument , 0 , 'b'},
    {"end"           , required_argument , 0 , 'e'},
    {"verbose"   , no_argument           , 0 , 'v'},
    {"method"        , required_argument , 0 , 'm'},
    {"regula"        , no_argument           , 0 , 'r'},
    {"linear"        , no_argument           , 0 , 'l'},
    {"tolerance" , required_argument , 0 , 't'},
    {"output"        , required_argument , 0 , 'o'},
    {0                   , 0                                 , 0 ,  0 }
};

using namespace std;
using namespace FlagControl;
using namespace num_methods;
using namespace num_methods::equations;

int main(int argc, char **argv) {
    char c;
    int option_index, steps;
    double a, b, tolerance;

    FlagController flag_controller;
    Function             fun;
    string               function;
    string               output;

    ofstream    foutput;

    while ((c = getopt_long(argc, argv, "f:s:b:e:vm:t:o:rl", long_options, &option_index)) != -1) {
        switch (c) {
            case 'f':
                function = optarg;
                flag_controller.add_flag(FUNCTION_GIVEN);
                break;
            case 's':
                steps  = stoi(optarg);
                flag_controller.add_flag(STEPS_GIVEN);
                break;
            case 'b':
                a = stod(optarg);
                flag_controller.add_flag(BEGIN_GIVEN);
                break;
            case 'e':
                b = stod(optarg);
                flag_controller.add_flag(END_GIVEN);
                break;
            case 'v':
                flag_controller.add_flag(VERBOSE);
                break;
            case 't':
                tolerance = stod(optarg);
                flag_controller.add_flag(TOLERANCE_GIVEN);
                break;
            case 'm':
                if (strcmp("1", optarg) == 0)
                    flag_controller.add_flag(FIRST_METHOD);
                else if (strcmp("2", optarg) == 0)
                    flag_controller.add_flag(SECOND_METHOD);
                else if (strcmp("3", optarg) == 0)
                    flag_controller.add_flag(THIRD_METHOD);
                else
                    return 1;
                flag_controller.add_flag(METHOD_GIVEN);
                break;
            case 'o':
                if (optarg)
                    output = optarg;
                else
                    return 1;
                flag_controller.add_flag(OUTPUT_GIVEN);
                break;
            case 'l':
                flag_controller.add_flag(LINEAR_METHOD);
                break;
            case 'r':
                flag_controller.add_flag(REGULA_METHOD);
                break;
            default:
                break;
        }
    }

    if (!flag_controller.has_flag(METHOD_GIVEN))
        flag_controller.add_flag(FIRST_METHOD);

    if (!flag_controller.has_flag(LINEAR_METHOD) || !flag_controller.has_flag(REGULA_METHOD))
        flag_controller.add_flag(REGULA_METHOD);

    bool verbose = flag_controller.has_flag(VERBOSE);

    fun.parse(function, "x");
    if (flag_controller.has_flag(OUTPUT_GIVEN))
        foutput.open(output);

    if (flag_controller.has_flag(FIRST_METHOD)) {
        if (!flag_controller.has_flag(NEEDED_OPTIONS | STEPS_GIVEN)) {
            cerr << "Missing steps for the first method!\n";
            return 1;
        }

        if (flag_controller.has_flag(LINEAR_METHOD)) {
            if (flag_controller.has_flag(OUTPUT_GIVEN))
                bisection_with_steps(fun, a, b, steps, verbose, foutput);
            else
                bisection_with_steps(fun, a, b, steps, verbose);
        } else {
            if (flag_controller.has_flag(OUTPUT_GIVEN))
                regula_with_steps(fun, a, b, steps, verbose, foutput);
            else
                regula_with_steps(fun, a, b, steps, verbose);
        }

    } else if (flag_controller.has_flag(SECOND_METHOD)) {

        if (!flag_controller.has_flag(NEEDED_OPTIONS | TOLERANCE_GIVEN)) {
            cerr << "Missing tolerance for the second method!\n";
            return 1;
        }

        if (flag_controller.has_flag(LINEAR_METHOD)) {
            if (flag_controller.has_flag(OUTPUT_GIVEN))
                bisection_with_img_tol(fun, a, b, tolerance, verbose, foutput);
            else
                bisection_with_img_tol(fun, a, b, tolerance, verbose);
        } else {
            if (flag_controller.has_flag(OUTPUT_GIVEN))
                regula_with_img_tol(fun, a, b, tolerance, verbose, foutput);
            else
                regula_with_img_tol(fun, a, b, tolerance, verbose);
        }
    } else if (flag_controller.has_flag(THIRD_METHOD)) {

        if (!flag_controller.has_flag(NEEDED_OPTIONS | TOLERANCE_GIVEN)) {
            cerr << "Missing tolerance for the third method!\n";
            return 1;
        }

        if (flag_controller.has_flag(LINEAR_METHOD)) {
            if (flag_controller.has_flag(OUTPUT_GIVEN))
                bisection_with_int_tol(fun, a, b, tolerance, verbose, foutput);
            else
                bisection_with_int_tol(fun, a, b, tolerance, verbose);
        } else {
            if (flag_controller.has_flag(OUTPUT_GIVEN))
                regula_with_err_tol(fun, a, b, tolerance, verbose, foutput);
            else
                regula_with_err_tol(fun, a, b, tolerance, verbose);
        }
    }
}
